package threads.lite.relay;

import java.nio.ByteBuffer;
import java.util.Objects;

import circuit.pb.Circuit;
import threads.lite.IPFS;
import threads.lite.cid.PeerId;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.utils.DataHandler;

public class RelayStopHandler implements ProtocolHandler {

    @Override
    public String getProtocol() {
        return IPFS.RELAY_PROTOCOL_STOP;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.RELAY_PROTOCOL_STOP));
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {

        Circuit.StopMessage stopMessage = Circuit.StopMessage.parseFrom(data.array());
        Objects.requireNonNull(stopMessage);

        if (stopMessage.hasPeer()) {
            PeerId peerId = PeerId.create(stopMessage.getPeer().getId().toByteArray());
            Circuit.StopMessage.Builder builder =
                    Circuit.StopMessage.newBuilder()
                            .setType(Circuit.StopMessage.Type.STATUS);
            builder.setStatus(Circuit.Status.OK);
            stream.setAttribute(StreamHandler.PEER, peerId);
            stream.writeOutput(DataHandler.encode(builder.build()));
        } else {
            Circuit.StopMessage.Builder builder =
                    Circuit.StopMessage.newBuilder()
                            .setType(Circuit.StopMessage.Type.STATUS);
            builder.setStatus(Circuit.Status.MALFORMED_MESSAGE);
            stream.writeOutput(DataHandler.encode(builder.build()));
        }

    }
}
