/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns.record;

import androidx.annotation.NonNull;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Inet4Address;

/**
 * A record payload (ip pointer).
 */
public class A extends InternetAddressRR<Inet4Address> {


    public A(byte[] ip) {
        super(ip);
        if (ip.length != 4) {
            throw new IllegalArgumentException("IPv4 address in A record is always 4 byte");
        }
    }

    public static A parse(DataInputStream dis) throws IOException {
        byte[] ip = new byte[4];
        dis.readFully(ip);
        return new A(ip);
    }

    @NonNull
    @Override
    public String toString() {
        return (ip[0] & 0xff) + "." +
                (ip[1] & 0xff) + "." +
                (ip[2] & 0xff) + "." +
                (ip[3] & 0xff);
    }

}
