package threads.lite.bitswap;

import java.nio.ByteBuffer;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.utils.DataHandler;

public class BitSwapHandler implements ProtocolHandler {

    private final Session session;

    public BitSwapHandler(Session session) {
        this.session = session;
    }

    @Override
    public String getProtocol() {
        return IPFS.BITSWAP_PROTOCOL;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.BITSWAP_PROTOCOL))
                .thenApply(Stream::closeOutput);
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        session.receiveMessage(stream.getConnection(),
                MessageOuterClass.Message.parseFrom(data.array()));
    }
}
