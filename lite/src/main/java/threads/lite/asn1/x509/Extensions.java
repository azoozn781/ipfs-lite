package threads.lite.asn1.x509;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Objects;
import java.util.Vector;

import threads.lite.asn1.ASN1Encodable;
import threads.lite.asn1.ASN1EncodableVector;
import threads.lite.asn1.ASN1Object;
import threads.lite.asn1.ASN1ObjectIdentifier;
import threads.lite.asn1.ASN1Primitive;
import threads.lite.asn1.ASN1Sequence;
import threads.lite.asn1.DERSequence;

/**
 * <pre>
 *     Extensions        ::=   SEQUENCE SIZE (1..MAX) OF Extension
 *
 *     Extension         ::=   SEQUENCE {
 *        extnId            EXTENSION.&amp;id ({ExtensionSet}),
 *        critical          BOOLEAN DEFAULT FALSE,
 *        extnValue         OCTET STRING }
 * </pre>
 */
public class Extensions extends ASN1Object {
    private final Hashtable<ASN1ObjectIdentifier, Extension> extensions = new Hashtable<>();
    private final Vector<ASN1ObjectIdentifier> ordering = new Vector<>();

    /**
     * Constructor from ASN1Sequence.
     * <p>
     * The extensions are a list of constructed sequences, either with (OID, OctetString) or (OID, Boolean, OctetString)
     * </p>
     */
    private Extensions(ASN1Sequence seq) {
        Enumeration<ASN1Encodable> e = seq.getObjects();

        while (e.hasMoreElements()) {
            Extension ext = Extension.getInstance(e.nextElement());
            Objects.requireNonNull(ext);
            if (extensions.containsKey(ext.getExtnId())) {
                throw new IllegalArgumentException("repeated extension found: " + ext.getExtnId());
            }

            extensions.put(ext.getExtnId(), ext);
            ordering.addElement(ext.getExtnId());
        }
    }

    /**
     * Base Constructor
     *
     * @param extensions an array of extensions.
     */
    public Extensions(Extension[] extensions) {
        for (int i = 0; i != extensions.length; i++) {
            Extension ext = extensions[i];

            this.ordering.addElement(ext.getExtnId());
            this.extensions.put(ext.getExtnId(), ext);
        }
    }

    public static Extensions getInstance(Object obj) {
        if (obj instanceof Extensions) {
            return (Extensions) obj;
        } else if (obj != null) {
            return new Extensions(ASN1Sequence.getInstance(obj));
        }

        return null;
    }

    /**
     * return the extension represented by the object identifier
     * passed in.
     *
     * @return the extension if it's present, null otherwise.
     */
    public Extension getExtension(
            ASN1ObjectIdentifier oid) {
        return extensions.get(oid);
    }

    /**
     * <pre>
     *     Extensions        ::=   SEQUENCE SIZE (1..MAX) OF Extension
     *
     *     Extension         ::=   SEQUENCE {
     *        extnId            EXTENSION.&amp;id ({ExtensionSet}),
     *        critical          BOOLEAN DEFAULT FALSE,
     *        extnValue         OCTET STRING }
     * </pre>
     */
    public ASN1Primitive toASN1Primitive() {
        ASN1EncodableVector vec = new ASN1EncodableVector(ordering.size());

        Enumeration<ASN1ObjectIdentifier> e = ordering.elements();
        while (e.hasMoreElements()) {
            ASN1ObjectIdentifier oid = e.nextElement();
            Extension ext = extensions.get(oid);

            vec.add(ext);
        }

        return new DERSequence(vec);
    }

}
