package threads.lite.autonat;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.ident.IdentityService;
import threads.lite.utils.DataHandler;

public class AutonatService {

    private static final String TAG = AutonatService.class.getSimpleName();

    public static void autonat(Session session, Set<Multiaddr> addresses,
                               Autonat autonat, Set<Multiaddr> multiaddrs) {
        ExecutorService service = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());


        Set<ConnectionObserved> connections = getAutonatConnections(session, autonat, multiaddrs);
        if (!autonat.abort()) {

            for (ConnectionObserved connection : connections) {

                service.execute(() -> {
                    try {
                        if (!autonat.abort()) {
                            autonat.addAddr(AutonatService.autonat(
                                            connection, session.self(), addresses)
                                    .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS));
                        }
                    } catch (Throwable ignore) {
                    }
                });
            }
        }
        service.shutdown();
        try {
            boolean termination = service.awaitTermination(IPFS.AUTONAT_TIMEOUT, TimeUnit.SECONDS);
            if (!termination) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
    }

    @NonNull
    public static Set<ConnectionObserved> getAutonatConnections(
            Session session, Autonat autonat, Set<Multiaddr> multiaddrs) {

        ExecutorService service = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());

        Set<ConnectionObserved> connections = ConcurrentHashMap.newKeySet();

        for (Multiaddr multiaddr : multiaddrs) {

            service.execute(() -> {
                try {
                    if (!autonat.abort()) {
                        Connection conn = session.connect(multiaddr, Parameters.getDefault());

                        // TODO this can be optimized, when connection already knows
                        // TODO  if it supports autonat (Feature RTT0.5, identity push)
                        PeerInfo peerInfo = IdentityService.getPeerInfo(session.self(), conn)
                                .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

                        if (peerInfo.hasProtocol(IPFS.AUTONAT_PROTOCOL)) {
                            Multiaddr observed = peerInfo.getObserved();
                            autonat.evaluateNatType(observed, conn.getLocalAddress());
                            connections.add(new ConnectionObserved(conn, observed));
                        }
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable.getMessage());
                }
            });
        }
        service.shutdown();
        try {
            boolean termination = service.awaitTermination(IPFS.AUTONAT_TIMEOUT, TimeUnit.SECONDS);
            if (!termination) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
        return connections;

    }

    public static CompletableFuture<Multiaddr> autonat(ConnectionObserved connectionObserved,
                                                       PeerId self, Set<Multiaddr> addresses) {
        CompletableFuture<Multiaddr> multiaddrCompletableFuture = new CompletableFuture<>();

        AutonatService.dial(connectionObserved, self, addresses).whenComplete((message, throwable) -> {
            if (throwable != null) {
                multiaddrCompletableFuture.completeExceptionally(throwable);
            } else {
                if (message.hasDialResponse()) {
                    autonat.pb.Autonat.Message.DialResponse dialResponse =
                            message.getDialResponse();
                    if (dialResponse.hasStatusText()) {
                        LogUtils.error(TAG, "Autonat Dial Text : "
                                + dialResponse.getStatusText());
                    }
                    if (dialResponse.hasStatus()) {
                        if (dialResponse.getStatus() ==
                                autonat.pb.Autonat.Message.ResponseStatus.OK) {
                            try {
                                ByteString raw = dialResponse.getAddr();
                                multiaddrCompletableFuture.complete(
                                        Multiaddr.create(self, raw.asReadOnlyByteBuffer()));
                            } catch (Throwable exception) {
                                multiaddrCompletableFuture.completeExceptionally(exception);
                            }
                        } else {
                            multiaddrCompletableFuture.completeExceptionally(
                                    new Exception(dialResponse.getStatusText())
                            );
                        }
                    } else {
                        multiaddrCompletableFuture.completeExceptionally(
                                new Exception("invalid status")
                        );
                    }
                }
            }
        });

        return multiaddrCompletableFuture;
    }

    @NonNull
    private static CompletableFuture<autonat.pb.Autonat.Message> dial(
            ConnectionObserved connectionObserved, PeerId peerId, Set<Multiaddr> addresses) {

        CompletableFuture<autonat.pb.Autonat.Message> done = new CompletableFuture<>();
        autonat.pb.Autonat.Message.PeerInfo.Builder peerInfoBuilder =
                autonat.pb.Autonat.Message.PeerInfo.newBuilder();

        peerInfoBuilder.setId(ByteString.copyFrom(peerId.encoded()));

        if (addresses.isEmpty()) {
            addresses.add(connectionObserved.observed);
        }

        for (Multiaddr addr : addresses) {
            peerInfoBuilder.addAddrs(ByteString.copyFrom(addr.encoded()));
        }

        autonat.pb.Autonat.Message.Dial dial = autonat.pb.Autonat.Message.Dial.newBuilder()
                .setPeer(peerInfoBuilder.build()).build();

        autonat.pb.Autonat.Message message = autonat.pb.Autonat.Message.newBuilder().
                setType(autonat.pb.Autonat.Message.MessageType.DIAL).
                setDial(dial).build();


        connectionObserved.connection.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                done.completeExceptionally(throwable);
            }

            @Override
            public void protocol(Stream stream, String protocol) throws Exception {
                if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL, IPFS.AUTONAT_PROTOCOL).contains(protocol)) {
                    throw new Exception("Token " + protocol + " not supported");
                }
                if (Objects.equals(protocol, IPFS.AUTONAT_PROTOCOL)) {
                    stream.writeOutput(DataHandler.encode(message)).
                            thenApply(Stream::closeOutput);
                }
            }

            @Override
            public void streamTerminated() {
                if (!done.isDone()) {
                    done.completeExceptionally(new Throwable("stream terminated"));
                }
            }

            @Override
            public void data(Stream stream, ByteBuffer data) throws Exception {
                done.complete(autonat.pb.Autonat.Message.parseFrom(data.array()));
            }
        }).whenComplete((stream, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                stream.writeOutput(DataHandler.encodeProtocols(
                        IPFS.MULTISTREAM_PROTOCOL, IPFS.AUTONAT_PROTOCOL));
            }
        });

        return done;

    }

    public static class ConnectionObserved {
        private final Connection connection;
        private final Multiaddr observed;

        public ConnectionObserved(Connection connection, Multiaddr observed) {
            this.connection = connection;
            this.observed = observed;
        }
    }
}
