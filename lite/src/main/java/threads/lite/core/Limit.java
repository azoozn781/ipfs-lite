package threads.lite.core;

import androidx.annotation.NonNull;

public class Limit {
    private final long data;
    private final long duration;
    private final Kind kind;

    public Limit(long data, long duration, Kind kind) {
        this.data = data;
        this.duration = duration;
        this.kind = kind;
    }

    public long getData() {
        return data;
    }

    public long getDuration() {
        return duration;
    }

    public Kind getKind() {
        return kind;
    }

    @NonNull
    @Override
    public String toString() {
        return "Limit{" +
                "data=" + data +
                ", duration=" + duration +
                ", kind=" + kind +
                '}';
    }

    public enum Kind {
        LIMITED, STATIC
    }
}
