package threads.lite.core;

import androidx.annotation.NonNull;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;

public class Reservation {
    @NonNull
    private final Limit limit;
    @NonNull
    private final Connection connection;
    @NonNull
    private final Multiaddr relayAddress;
    @NonNull
    private final Multiaddr circuitAddress;

    private final Date expire;

    public Reservation(@NonNull Limit limit, @NonNull Connection connection,
                       @NonNull Multiaddr relayAddress, @NonNull Multiaddr circuitAddress,
                       long expire) {
        this.limit = limit;
        this.connection = connection;
        this.relayAddress = relayAddress;
        this.circuitAddress = circuitAddress;
        this.expire = new Date(expire * 1000);
    }


    // when 0, there is not limitation of bytes during communication
    public long getLimitData() {
        return limit.getData();
    }

    // when 0, there is no time limitation
    public long getLimitDuration() {
        return limit.getDuration();
    }

    @NonNull
    public Multiaddr circuitMultiaddr() {
        return circuitAddress;
    }


    @NonNull
    public Multiaddr getRelayAddress() {
        return relayAddress;
    }


    public long expireInMinutes() {
        Date now = new Date();
        long duration = expire.getTime() - now.getTime();
        return TimeUnit.MILLISECONDS.toMinutes(duration);
    }


    @NonNull
    @Override
    public String toString() {
        return "Reservation{" +
                " limit=" + limit +
                ", relayAddress=" + relayAddress +
                ", expire=" + expire +
                '}';
    }

    @NonNull
    public Limit.Kind getKind() {
        return limit.getKind();
    }

    @NonNull
    public PeerId getRelayId() {
        return getRelayAddress().getPeerId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return connection.equals(that.connection) &&
                relayAddress.equals(that.relayAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connection, relayAddress);
    }

    @NonNull
    public Connection getConnection() {
        return connection;
    }

    public boolean isStaticRelay() {
        return getKind() == Limit.Kind.STATIC;
    }


}
