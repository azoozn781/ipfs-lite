/*
 * Copyright (C) 2016 Southern Storm Software, Pty Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package threads.lite.noise.protocol;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;

import threads.lite.noise.CipherState;

/**
 * Utility functions for the Noise protocol library.
 */
public final class NoiseUtility {

    /**
     * Creates a Diffie-Hellman object from its Noise protocol name.
     *
     * @param name The name of the DH algorithm; e.g. "25519", "448", etc.
     * @return The Diffie-Hellman object if the name is recognized.
     * @throws NoSuchAlgorithmException The name is not recognized as a
     *                                  valid Noise protocol name, or there is no cryptography provider
     *                                  in the system that implements the algorithm.
     */
    public static DHState createDH(String name) throws NoSuchAlgorithmException {
        if (name.equals("25519"))
            return new DHState();
        throw new NoSuchAlgorithmException("Unknown Noise DH algorithm name: " + name);
    }

    /**
     * Creates a cipher object from its Noise protocol name.
     *
     * @param name The name of the cipher algorithm; e.g. "AESGCM", "ChaChaPoly", etc.
     * @return The cipher object if the name is recognized.
     * @throws NoSuchAlgorithmException The name is not recognized as a
     *                                  valid Noise protocol name, or there is no cryptography provider
     *                                  in the system that implements the algorithm.
     */
    public static CipherState createCipher(String name) throws NoSuchAlgorithmException {

        if (name.equals("ChaChaPoly")) {
            return new ChaChaPolyCipherState();
        }
        throw new NoSuchAlgorithmException("Unknown Noise cipher algorithm name: " + name);
    }

    /**
     * Creates a hash object from its Noise protocol name.
     *
     * @param name The name of the hash algorithm; e.g. "SHA256", "BLAKE2s", etc.
     * @return The hash object if the name is recognized.
     * @throws NoSuchAlgorithmException The name is not recognized as a
     *                                  valid Noise protocol name, or there is no cryptography provider
     *                                  in the system that implements the algorithm.
     */
    public static MessageDigest createHash(String name) throws NoSuchAlgorithmException {
        // Look for a JCA/JCE provider first and if that doesn't work,
        // use the fallback implementations in this library instead.
        // The only algorithm that is required to be implemented by a
        // JDK is "SHA-256", although "SHA-512" is fairly common as well.
        switch (name) {
            case "SHA256":
                return MessageDigest.getInstance("SHA-256");
            case "SHA512":
                return MessageDigest.getInstance("SHA-512");

        }
        throw new NoSuchAlgorithmException("Unknown Noise hash algorithm name: " + name);
    }

    // The rest of this class consists of internal utility functions
    // that are not part of the public API.

    /**
     * Destroys the contents of a byte array.
     *
     * @param array The array whose contents should be destroyed.
     */
    static void destroy(byte[] array) {
        Arrays.fill(array, (byte) 0);
    }

    /**
     * Throws an instance of AEADBadTagException.
     *
     * @throws BadPaddingException The AEAD exception.
     *                             <p>
     *                             If the underlying JDK does not have the AEADBadTagException
     *                             class, then this function will instead throw an instance of
     *                             the superclass BadPaddingException.
     */
    static void throwBadTagException() throws BadPaddingException {
        try {
            Class<?> c = Class.forName("javax.crypto.AEADBadTagException");
            throw (BadPaddingException) (c.newInstance());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ignored) {
        }
        throw new BadPaddingException();
    }
}
