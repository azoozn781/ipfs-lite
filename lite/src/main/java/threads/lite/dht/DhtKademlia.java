package threads.lite.dht;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import dht.pb.Dht;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.ID;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.Host;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Parameters;
import threads.lite.core.Routing;
import threads.lite.core.Server;
import threads.lite.ipns.IpnsService;


public class DhtKademlia implements Routing, AutoCloseable {

    private static final String TAG = DhtKademlia.class.getSimpleName();

    @NonNull
    public final RoutingTable routingTable = new RoutingTable();
    @NonNull
    public final Host host;
    @NonNull
    private final ReentrantLock lock = new ReentrantLock();
    @NonNull
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public DhtKademlia(@NonNull Host host) {
        this.host = host;
    }

    void bootstrap() {
        if (routingTable.isEmpty()) {
            lock.lock();
            try {
                Set<Peer> peers = host.getRoutingPeers();
                for (Peer peer : peers) {
                    routingTable.addPeer(peer);
                }
                List<Peer> randomPeers = host.getPeerStore().getRandomPeers(IPFS.DHT_TABLE_SIZE);

                for (Peer peer : randomPeers) {
                    peer.setBootstrap(true);
                    if (peer.getMultiaddr().protocolSupported(host.getResolver().ipv().get())) {
                        routingTable.addPeer(peer);
                    }
                }
            } finally {
                lock.unlock();
            }
        }
    }


    @NonNull
    private List<Peer> evalClosestPeers(@NonNull Dht.Message pms) {

        List<Peer> peers = new ArrayList<>();
        List<Dht.Message.Peer> list = pms.getCloserPeersList();

        for (Dht.Message.Peer entry : list) {

            PeerId peerId = PeerId.create(entry.getId().toByteArray());

            Multiaddr resolved = host.getResolver().resolveToOne(peerId, entry.getAddrsList());
            if (resolved != null) {
                try {
                    peers.add(Peer.create(resolved));
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

        }
        return peers;
    }


    @Override
    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Multiaddr> consumer,
                                 @NonNull PeerId peerId) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();
            Set<Multiaddr> handled = ConcurrentHashMap.newKeySet();
            byte[] target = peerId.encoded();
            ID key = ID.convertPeerID(peerId);

            Dht.Message findNodeMessage = DhtService.createFindNodeMessage(target);
            getClosestPeers(done, key, findNodeMessage, peers -> {
                for (Peer peer : peers) {
                    if (handled.add(peer.getMultiaddr())) {
                        consumer.accept(peer.getMultiaddr());
                    }
                }
            });
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private void getClosestPeers(@NonNull Cancellable cancellable, @NonNull ID key,
                                 @NonNull Dht.Message message, @NonNull Consumer<List<Peer>> channel)
            throws InterruptedException {

        runQuery(cancellable, key, (ctx1, peer) -> {

            Dht.Message pms = sendRequest(ctx1, peer, message);

            List<Peer> peers = evalClosestPeers(pms);

            channel.accept(peers);

            return peers;
        });

    }


    @Override
    public void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull byte[] target, @NonNull byte[] value) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();

            // don't allow local users to put bad values.
            IpnsEntity entry = IpnsService.validate(IPFS.DHT_PROTOCOL, target, value);
            Objects.requireNonNull(entry);

            Dht.Message putValueMessage = DhtService.createPutValueMessage(target, value);
            Dht.Message findNodeMessage = DhtService.createFindNodeMessage(target);

            Set<Multiaddr> handled = ConcurrentHashMap.newKeySet();
            ID key = ID.convertKey(target);
            getClosestPeers(done, key, findNodeMessage, peers -> {

                for (Peer peer : peers) {
                    if (handled.add(peer.getMultiaddr())) {
                        try {
                            Dht.Message res = sendRequest(cancellable, peer, putValueMessage);
                            Objects.requireNonNull(res);

                            if (res.hasRecord()) {
                                // no extra validation, should be fine
                                consumer.accept(peer.getMultiaddr());
                            }

                        } catch (Throwable ignore) {
                        }
                    }
                }
            });
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @Override
    public void findProviders(@NonNull Cancellable cancellable,
                              @NonNull Consumer<Multiaddr> consumer,
                              @NonNull Cid cid) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();

            Dht.Message message = DhtService.createGetProvidersMessage(cid.getMultihash());

            byte[] multihash = cid.getMultihash().encoded();
            ID key = ID.convertKey(multihash);

            Set<Multiaddr> handled = ConcurrentHashMap.newKeySet();
            runQuery(done, key, (ctx, p) -> {

                Dht.Message pms = sendRequest(ctx, p, message);

                List<Dht.Message.Peer> list = pms.getProviderPeersList();

                for (Dht.Message.Peer entry : list) {

                    if (done.isCancelled()) {
                        throw new InterruptedException();
                    }

                    if (entry.getAddrsCount() > 0) {
                        PeerId peerId = PeerId.create(entry.getId().toByteArray());
                        // contains circuit addresses
                        Multiaddr multiaddr = host.getResolver().resolveToOne(
                                peerId, entry.getAddrsList());
                        if (multiaddr != null) {
                            if (handled.add(multiaddr)) {
                                consumer.accept(multiaddr);
                            }
                        }
                    }
                }

                return evalClosestPeers(pms);

            });
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void addToRouting(@NonNull QueryPeer queryPeer) {

        Peer peer = queryPeer.getPeer();
        boolean added = routingTable.addPeer(peer);


        // only replaceable peer are adding
        // (the replaceable peers are initial the routing peers)
        if (added && peer.isReplaceable()) {
            host.getPeerStore().storePeer(peer);
        }
    }

    public void removeFromRouting(QueryPeer peer, boolean removeBootstrap) {
        boolean result = routingTable.removePeer(peer);
        if (result) {
            LogUtils.info(TAG, "Remove from routing " + peer);
        }
        if (removeBootstrap) {
            if (peer.getPeer().isBootstrap()) {
                // remove from bootstrap
                host.getPeerStore().removePeer(peer.getPeer());
            }
        }
    }


    public void provide(@NonNull Cancellable cancellable,
                        @NonNull Server server,
                        @NonNull Consumer<Multiaddr> consumer,
                        @NonNull Cid cid) {


        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();

            byte[] multihash = cid.getMultihash().encoded();
            ID key = ID.convertKey(multihash);
            Dht.Message findNodeMessage = DhtService.createFindNodeMessage(multihash);
            Dht.Message message = DhtService.createAddProviderMessage(server, cid.getMultihash());

            Set<Multiaddr> handled = ConcurrentHashMap.newKeySet();

            getClosestPeers(done, key, findNodeMessage, peers -> {

                ExecutorService executorService = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());

                for (Peer peer : peers) {
                    if (handled.add(peer.getMultiaddr())) {
                        executorService.execute(() -> {
                            try {
                                if (cancellable.isCancelled()) {
                                    throw new InterruptedException();
                                }

                                Connection connection = host.connect(peer.getMultiaddr(),
                                        Parameters.getDefault());

                                if (cancellable.isCancelled()) {
                                    throw new InterruptedException();
                                }
                                sendMessage(connection, message);
                                // success assumed
                                consumer.accept(connection.remoteMultiaddr());

                            } catch (Throwable ignore) {
                            }
                        });
                    }
                }

                executorService.shutdown();

                try {
                    boolean termination = executorService.awaitTermination(
                            cancellable.timeout(), TimeUnit.SECONDS);
                    if (!termination) {
                        executorService.shutdownNow();
                    }
                } catch (Throwable ignore) {
                }
            });
        } catch (InterruptedException ignore) {
            // ignore standard exception here
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private void sendMessage(@NonNull Connection connection, @NonNull Dht.Message message)
            throws ExecutionException, InterruptedException, TimeoutException {
        DhtService.sendMessage(connection, message).get(
                IPFS.DHT_REQUEST_TIMEOUT, TimeUnit.SECONDS);

    }

    private Dht.Message makeRequest(@NonNull Connection connection, @NonNull Peer peer,
                                    @NonNull Dht.Message message)
            throws InterruptedException, ExecutionException, TimeoutException {

        try {
            return DhtService.request(connection, message)
                    .get(IPFS.DHT_REQUEST_TIMEOUT, TimeUnit.SECONDS);
        } finally {
            peer.setRtt(connection.getSmoothedRtt());
        }
    }

    @NonNull
    private Dht.Message sendRequest(@NonNull Cancellable cancellable, @NonNull Peer peer,
                                    @NonNull Dht.Message message)
            throws InterruptedException, ConnectException, TimeoutException {


        if (cancellable.isCancelled()) {
            throw new InterruptedException();
        }

        try {
            Connection connection = host.connect(peer.getMultiaddr(), Parameters.getDefault());
            if (cancellable.isCancelled()) {
                throw new InterruptedException();
            }
            return makeRequest(connection, peer, message);
        } catch (ExecutionException exception) {
            throw new ConnectException(exception.getClass().getSimpleName());
        }
    }


    @Override
    public void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId id) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();

            byte[] target = id.encoded();
            Dht.Message findNodeMessage = DhtService.createFindNodeMessage(target);
            ID key = ID.convertKey(target);

            Set<Multiaddr> handled = ConcurrentHashMap.newKeySet();
            runQuery(done, key, (ctx, peer) -> {

                Dht.Message pms = sendRequest(ctx, peer, findNodeMessage);

                List<Peer> peers = new ArrayList<>();
                List<Dht.Message.Peer> list = pms.getCloserPeersList();
                for (Dht.Message.Peer entry : list) {

                    PeerId peerId = PeerId.create(entry.getId().toByteArray());
                    if (Objects.equals(peerId, id)) {
                        // contains circuit addresses
                        Multiaddrs multiaddrs = host.getResolver().resolve(
                                peerId, entry.getAddrsList());
                        for (Multiaddr multiaddr : multiaddrs) {
                            if (handled.add(multiaddr)) {
                                consumer.accept(multiaddr);
                            }
                        }
                    } else {
                        Multiaddr resolved = host.getResolver().resolveToOne(
                                peerId, entry.getAddrsList());
                        if (resolved != null) {
                            try {
                                peers.add(Peer.create(resolved));
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    }
                }
                return peers;

            });
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void runQuery(@NonNull Cancellable cancellable, @NonNull ID key,
                          @NonNull QueryFunc queryFn) throws InterruptedException {

        // pick the K closest peers to the key in our Routing table.
        List<QueryPeer> seedPeers = routingTable.nearestPeers(key);
        Query.runQuery(this, cancellable, key, seedPeers, queryFn);

    }

    private List<Peer> getRecordOfPeers(@NonNull Cancellable cancellable,
                                        @NonNull Peer peer,
                                        @NonNull Consumer<IpnsEntity> consumer,
                                        @NonNull Dht.Message message)
            throws InterruptedException, ConnectException, TimeoutException {


        Dht.Message pms = sendRequest(cancellable, peer, message);

        List<Peer> peers = evalClosestPeers(pms);

        if (pms.hasRecord()) {

            Dht.Message.Record rec = pms.getRecord();
            try {
                byte[] record = rec.getValue().toByteArray();
                if (record != null && record.length > 0) {
                    IpnsEntity entry = IpnsService.validate(IPFS.DHT_PROTOCOL,
                            rec.getKey().toByteArray(), record);
                    consumer.accept(entry);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage());
            }
        }

        return peers;
    }


    private void processValues(@Nullable IpnsEntity best,
                               @NonNull IpnsEntity current,
                               @NonNull Consumer<IpnsEntity> reporter) {

        if (best != null) {
            int value = IpnsService.compare(best, current);
            if (value == -1) { // "current" is newer entry
                reporter.accept(current);
            }
        } else {
            reporter.accept(current);
        }
    }

    @Override
    public void searchValue(@NonNull Cancellable cancellable,
                            @NonNull Consumer<IpnsEntity> consumer,
                            @NonNull byte[] target) {

        try {
            Cancellable done = () -> cancellable.isCancelled() || isClosed();
            bootstrap();
            Dht.Message message = DhtService.createGetValueMessage(target);
            AtomicReference<IpnsEntity> best = new AtomicReference<>();
            ID key = ID.convertKey(target);

            runQuery(done, key, (ctx1, peer) -> getRecordOfPeers(ctx1, peer,
                    entry -> processValues(best.get(), entry, (current) -> {
                        consumer.accept(current);
                        best.set(current);
                    }), message));
        } catch (InterruptedException ignore) {
            // ignore standard exception
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void close() {
        routingTable.clear();
        closed.set(true);
    }

    public boolean isClosed() {
        return closed.get();
    }


    public interface QueryFunc {
        @NonNull
        List<Peer> query(@NonNull Cancellable cancellable, @NonNull Peer peer)
                throws InterruptedException, ConnectException, TimeoutException;
    }
}
