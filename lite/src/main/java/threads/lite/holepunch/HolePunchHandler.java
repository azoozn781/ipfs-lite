package threads.lite.holepunch;

import java.nio.ByteBuffer;
import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.cid.PeerId;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Server;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.utils.DataHandler;

public class HolePunchHandler implements ProtocolHandler {

    private final Server server;

    public HolePunchHandler(Server server) {
        this.server = server;
    }

    @Override
    public String getProtocol() {
        return IPFS.HOLE_PUNCH_PROTOCOL;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.HOLE_PUNCH_PROTOCOL));
        HolePunch.initializeConnect(stream, server.getObserved());
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        PeerId peerId = Objects.requireNonNull((PeerId) stream.getAttribute(StreamHandler.PEER));
        HolePunch.sendSync(server, stream, peerId, data);
    }
}
