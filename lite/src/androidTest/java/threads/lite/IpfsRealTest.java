package threads.lite;


import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.blockstore.BlockStoreCache;
import threads.lite.cid.Cid;
import threads.lite.cid.Network;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;
import threads.lite.utils.TimeoutProgress;

@RunWith(AndroidJUnit4.class)
public class IpfsRealTest {
    private static final String TAG = IpfsRealTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_blog_ipfs_io() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = ipfs.createSession(true)) {
            String link = ipfs.resolveDnsLink("blog.ipfs.io");

            assertNotNull(link);
            assertFalse(link.isEmpty());
            Cid cid = Cid.decode(link.replace(IPFS.IPFS_PATH, ""));

            LogUtils.error(TAG, cid.toString());
            LogUtils.error(TAG, cid.getPrefix().toString());

            Cid node = ipfs.resolveCid(session, cid, Collections.emptyList(),
                    new TimeoutCancellable(60));
            assertNotNull(node);

            List<Link> links = ipfs.links(session, node, false, () -> false);
            assertNotNull(links);
            for (Link lnk : links) {
                LogUtils.info(TAG, lnk.toString());
            }

            node = ipfs.resolveCid(session, node, List.of(IPFS.INDEX_HTML),
                    new TimeoutCancellable(60));
            assertNotNull(node);

            String text = ipfs.getText(session, node, new TimeoutCancellable(30));

            assertNotNull(text);
            assertFalse(text.isEmpty());
        }

    }

    @Test
    public void stress_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (BlockStoreCache cache = BlockStoreCache.createInstance(context)) {

            try (Session session = ipfs.createSession(cache, true)) {

                File file = TestEnv.createCacheFile(context);

                AtomicInteger percent = new AtomicInteger(0);

                ipfs.fetchToFile(session, file, Cid.decode("QmcniBv7UQ4gGPQQW2BwbD4ZZHzN3o3tPuNLZCbBchd1zh"),
                        new TimeoutProgress(TimeUnit.MINUTES.toSeconds(5)) {
                            @Override
                            public void setProgress(int progress) {
                                LogUtils.info(TAG, "Progress " + progress);
                                percent.set(progress);
                            }

                            @Override
                            public boolean doProgress() {
                                return true;
                            }
                        });

                TestCase.assertEquals(percent.get(), 100);
            }
        }
    }


    @Test
    public void test_unknown() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = ipfs.createSession()) {

            Cid node = ipfs.resolveCid(session,
                    Cid.decode("QmavE42xtK1VovJFVTVkCR5Jdf761QWtxmvak9Zx718TVr"),
                    Collections.emptyList(), new TimeoutCancellable(60));
            assertNotNull(node);

            List<Link> links = ipfs.links(session, node, false,
                    new TimeoutCancellable(1));
            assertNotNull(links);
            assertFalse(links.isEmpty());
        }
    }

    @Test
    public void test_unknown_2() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = ipfs.createSession()) {
            Cid node = ipfs.resolveCid(session,
                    Cid.decode("QmfQiLpdBDbSkb2oySwFHzNucvLkHmGFxgK4oA2BUSwi4t"),
                    Collections.emptyList(), new TimeoutCancellable(60));
            assertNotNull(node);

            List<Link> links = ipfs.links(session, node, false,
                    new TimeoutCancellable(1));
            assertNotNull(links);
            assertFalse(links.isEmpty());
        }
    }

}
