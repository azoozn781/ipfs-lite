package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.Progress;
import threads.lite.core.Server;
import threads.lite.core.Session;

@RunWith(AndroidJUnit4.class)
public class IpfsClientServerTest {


    private static final String TAG = IpfsClientServerTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void server_stress_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        try (Session session = ipfs.createSession()) {
            Dummy dummy = Dummy.getInstance(context);

            PeerId host = ipfs.self();
            assertNotNull(host);
            Multiaddr multiaddr = Multiaddr.getLoopbackAddress(ipfs.self(), server.getPort());


            try (Session dummySession = dummy.createSession()) {


                // 10 MB
                byte[] input = TestEnv.getRandomBytes(10000000);

                Cid cid = ipfs.storeData(session, input);
                assertNotNull(cid);

                byte[] cmp = ipfs.getData(session, cid, () -> false);
                assertArrayEquals(input, cmp);

                List<Cid> cids = ipfs.getBlocks(session, cid);
                LogUtils.debug(TAG, "Links " + cids.size());


                Connection conn = dummySession.dial(multiaddr, Parameters.getDefault());
                Objects.requireNonNull(conn);

                Thread.sleep(1000);

                assertEquals(server.numServerConnections(), 1);

                PeerInfo info = dummy.getHost().getPeerInfo(conn).
                        get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                assertNotNull(info);
                assertEquals(info.getAgent(), IPFS.AGENT);
                assertNotNull(info.getObserved());


                byte[] output = dummy.getData(dummySession, cid, new Progress() {
                    @Override
                    public void setProgress(int progress) {
                        LogUtils.verbose(TAG, "" + progress);
                    }

                    @Override
                    public boolean isCancelled() {
                        return false;
                    }
                });
                assertArrayEquals(input, output);

                conn.close();

                Thread.sleep(1000);

            } finally {
                dummy.clearDatabase();
            }

            Thread.sleep(3000);
            assertEquals(server.numServerConnections(), 0);
        }

    }
}
