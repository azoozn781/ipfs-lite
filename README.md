# IPFS Lite

IPFS Lite is an application to support the standard use cases of IPFS

## General

The basic characteristics of the app are decentralized, respect of personal data,
open source, free of charge, transparent, free of advertising and legally impeccable.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
alt="Get it on F-Droid"
height="80">](https://f-droid.org/packages/threads.server/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
alt="Get it on Google Play"
height="80">](https://play.google.com/store/apps/details?id=threads.server)

## Documentation

**IPFS Lite** is a server only implementation. The related client application can be found at the
following location [Thor - IPFS Lite](https://gitlab.com/remmer.wilts/thor).

The reason for the strict separation between client and server, relies in performance reasons.

The client **Thor - IPFS Lite** is available for several app stores (like F-Droid, Google Play,
Huawei).

**IPFS Lite** based on a subset of IPFS (https://ipfs.io/), which is described in detail
in the **IPFS Lite Library** (https://gitlab.com/remmer.wilts/threads-lite/)

### Tips

Due to the fact that **IPFS** itself does not offer static relays anymore, it is a common
problem that **IPFS Lite** is not reachable from the outside.

The application itself tries to connect to relays, so that at least it can be reachable from the
outside via **hole punching**, in case when it is not public reachable anyway.

In case the application is not reachable, you might improve the reachability by switching
to **IPv6** or do **port forwarding** in case you are behind a router in WLAN mode.

**Hole punching** only will work, when the server is not behind a symmetric nat. In case
your are behind a symmetric nat, your application will only work in "LOCAL" mode,
which means it just works in the same local network (WLAN).

The reachability information will be presented to the user.

### Difference to IPFS (kubo)

TODO

## Links

[Privacy Policy](https://gitlab.com/remmer.wilts/ipfs-lite/-/blob/master/POLICY.md)
<br/>
[Apache License](https://gitlab.com/remmer.wilts/ipfs-lite/-/blob/master/LICENSE)
