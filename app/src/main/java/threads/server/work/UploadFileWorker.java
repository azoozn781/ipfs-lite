package threads.server.work;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.InputStream;
import java.util.Objects;
import java.util.UUID;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;
import threads.server.core.files.FileInfo;

public class UploadFileWorker extends Worker {
    private static final String TAG = UploadFileWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public UploadFileWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(DOCS.IDX, idx);

        return new OneTimeWorkRequest.Builder(UploadFileWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static UUID load(@NonNull Context context, long idx) {
        OneTimeWorkRequest request = getWork(idx);
        WorkManager.getInstance(context).enqueue(request);
        return request.getId();
    }


    @NonNull
    @Override
    public Result doWork() {


        long idx = getInputData().getLong(DOCS.IDX, -1);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);


        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());
            FILES files = FILES.getInstance(getApplicationContext());
            EVENTS events = EVENTS.getInstance(getApplicationContext());

            files.setLeaching(idx);
            files.setWork(idx, getId());

            FileInfo fileInfo = files.getFileInfo(idx);
            Objects.requireNonNull(fileInfo);

            String url = fileInfo.getUri();
            Objects.requireNonNull(url);
            Uri uri = Uri.parse(url);

            long size = fileInfo.getSize();

            // normal case like content of files

            try (Session session = ipfs.createSession(false)) {
                try (InputStream inputStream = getApplicationContext().getContentResolver()
                        .openInputStream(uri)) {
                    Objects.requireNonNull(inputStream);

                    Cid cid = ipfs.storeInputStream(session, inputStream, new Progress() {
                        @Override
                        public void setProgress(int progress) {

                        }

                        @Override
                        public boolean isCancelled() {
                            return isStopped();
                        }
                    }, size);

                    Objects.requireNonNull(cid);
                    files.setDone(idx, cid);
                    docs.finishDocument(idx);

                } catch (Throwable throwable) {
                    files.setDeleting(idx);
                    events.warning(getApplicationContext().getString(
                            R.string.download_canceled, fileInfo.getName()));
                    throw throwable;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

}
