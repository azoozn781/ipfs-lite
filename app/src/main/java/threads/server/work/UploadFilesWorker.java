package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;

public class UploadFilesWorker extends Worker {
    private static final String TAG = UploadFilesWorker.class.getSimpleName();
    private final NotificationManager notificationManager;

    @SuppressWarnings("WeakerAccess")
    public UploadFilesWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

        notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    public static OneTimeWorkRequest getWork(long parent, @NonNull File file) {

        Data.Builder data = new Data.Builder();
        data.putString(DOCS.FILE, file.getName());
        data.putLong(DOCS.IDX, parent);

        return new OneTimeWorkRequest.Builder(UploadFilesWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static void load(@NonNull Context context, long parent, @NonNull File file) {
        WorkManager.getInstance(context).enqueue(getWork(parent, file));
    }


    @NonNull
    @Override
    public Result doWork() {

        String filename = getInputData().getString(DOCS.FILE);
        long parent = getInputData().getLong(DOCS.IDX, 0L);
        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start ... ");

        try {
            FILES files = FILES.getInstance(getApplicationContext());
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());
            EVENTS events = EVENTS.getInstance(getApplicationContext());

            Objects.requireNonNull(filename);
            File file = DOCS.getTempFile(getApplicationContext(), filename);
            Objects.requireNonNull(file);
            int notificationId = getId().hashCode();

            List<String> uris = new ArrayList<>();
            try (BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file)))) {
                Objects.requireNonNull(reader);
                while (reader.ready()) {
                    uris.add(reader.readLine());
                }
            }

            int maxIndex = uris.size();
            AtomicInteger index = new AtomicInteger(0);


            Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                    InitApplication.STORAGE_CHANNEL_ID);


            PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                    .createCancelPendingIntent(getId());
            String cancel = getApplicationContext().getString(android.R.string.cancel);

            Intent main = new Intent(getApplicationContext(), MainActivity.class);

            int requestID = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                    main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            Notification.Action action = new Notification.Action.Builder(
                    Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                    intent).build();

            builder.setContentTitle(getApplicationContext().getString(R.string.uploading))
                    .setSubText("" + 0 + "/" + maxIndex)
                    .setContentIntent(pendingIntent)
                    .setProgress(100, 0, false)
                    .setOnlyAlertOnce(true)
                    .setSmallIcon(R.drawable.download)
                    .addAction(action)
                    .setCategory(Notification.CATEGORY_PROGRESS)
                    .setOngoing(true);

            Notification notification = builder.build();

            notificationManager.notify(notificationId, notification);
            setForegroundAsync(new ForegroundInfo(notificationId, notification));

            try (Session session = ipfs.createSession(false)) {

                for (String uriStr : uris) {
                    Uri uri = Uri.parse(uriStr);
                    if (!isStopped()) {
                        if (!DOCS.hasReadPermission(getApplicationContext(), uri)) {
                            EVENTS.getInstance(getApplicationContext()).error(
                                    getApplicationContext().getString(
                                            R.string.file_has_no_read_permission));
                            continue;
                        }

                        if (DOCS.isPartial(getApplicationContext(), uri)) {
                            EVENTS.getInstance(getApplicationContext()).error(
                                    getApplicationContext().getString(R.string.file_not_valid));
                            continue;
                        }

                        final int indexValue = index.incrementAndGet();


                        String name = DOCS.getFileName(getApplicationContext(), uri);
                        String mimeType = DOCS.getMimeType(getApplicationContext(), uri);

                        long size = DOCS.getFileSize(getApplicationContext(), uri);

                        long idx = docs.createDocument(parent, mimeType, null, uri,
                                name, size, false, true);

                        files.setWork(idx, getId());

                        try (InputStream inputStream = getApplicationContext().getContentResolver()
                                .openInputStream(uri)) {
                            Objects.requireNonNull(inputStream);

                            Cid cid = ipfs.storeInputStream(session, inputStream, new Progress() {

                                @Override
                                public boolean isCancelled() {
                                    return isStopped();
                                }

                                @Override
                                public void setProgress(int progress) {
                                    builder.setSubText("" + indexValue + "/" + maxIndex)
                                            .setContentTitle(name)
                                            .setProgress(100, progress, false);
                                    notificationManager.notify(notificationId, builder.build());
                                }
                            }, size);


                            Objects.requireNonNull(cid);
                            files.setDone(idx, cid);
                            docs.finishDocument(idx);
                        } catch (Throwable throwable) {
                            files.setDeleting(idx);
                            events.warning(getApplicationContext().getString(
                                    R.string.download_canceled, name));
                            throw throwable;
                        }
                    }
                }
            } finally {
                notificationManager.cancel(notificationId);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }


}
