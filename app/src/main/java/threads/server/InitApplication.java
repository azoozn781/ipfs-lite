package threads.server;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;

import androidx.annotation.NonNull;

import com.google.android.material.color.DynamicColors;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Server;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.services.DaemonService;
import threads.server.services.RegistrationService;

public class InitApplication extends Application {

    public static final String RESET_CHANNEL_ID = "RESET_CHANNEL_ID";
    public static final String STORAGE_CHANNEL_ID = "STORAGE_CHANNEL_ID";
    public static final String DAEMON_CHANNEL_ID = "DAEMON_CHANNEL_ID";
    private static final String APP_KEY = "LITE_KEY";
    private static final String TITLE_KEY = "TITLE_KEY";
    private static final String TAG = InitApplication.class.getSimpleName();

    private NsdManager nsdManager;

    public static void setTitle(@NonNull Context context, @NonNull String title) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TITLE_KEY, title);
        editor.apply();
    }

    @NonNull
    public static String getTitle(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(TITLE_KEY,
                context.getString(R.string.homepage)));

    }

    private void createCleanupChannel(@NonNull Context context) {

        try {
            CharSequence name = context.getString(R.string.application_reset);
            String description = context.getString(R.string.reset_application_data);
            NotificationChannel mChannel = new NotificationChannel(
                    RESET_CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void createStorageChannel(@NonNull Context context) {
        try {
            CharSequence name = context.getString(R.string.storage_channel_name);
            String description = context.getString(R.string.storage_channel_description);
            NotificationChannel mChannel = new NotificationChannel(
                    STORAGE_CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private void createDaemonChannel(@NonNull Context context) {
        try {
            CharSequence name = context.getString(R.string.daemon_channel_name);
            String description = context.getString(R.string.daemon_channel_description);
            NotificationChannel mChannel = new NotificationChannel(DAEMON_CHANNEL_ID, name,
                    NotificationManager.IMPORTANCE_LOW);

            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        DynamicColors.applyToActivitiesIfAvailable(this);

        createStorageChannel(getApplicationContext());
        createDaemonChannel(getApplicationContext());
        createCleanupChannel(getApplicationContext());

        registerService();

        DaemonService.start(getApplicationContext());
    }


    private void registerService() {
        EVENTS events = EVENTS.getInstance(getApplicationContext());
        try {

            nsdManager = (NsdManager) getSystemService(Context.NSD_SERVICE);
            Objects.requireNonNull(nsdManager);

            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());


            ipfs.setRecordSupplier(() -> {
                try {
                    IpnsEntity page = docs.getHomePage();
                    if (page != null) {
                        return ipfs.createSelfSignedIpnsRecord(page.getSequence(),
                                page.getValue(), page.getName());
                    }
                    return ipfs.createSelfSignedIpnsRecord(0, new byte[0], null);
                } catch (Throwable throwable) {
                    throw new RuntimeException(throwable);
                }
            });


            Server server = docs.getServer();
            PeerId peerId = ipfs.self();
            String ownServiceName = peerId.toString();


            NsdServiceInfo serviceInfo = new NsdServiceInfo();
            try {
                List<Multiaddr> addresses = Multiaddr.getSiteLocalAddresses(
                        ipfs.self(), server.getPort());
                Objects.requireNonNull(addresses);
                if (!addresses.isEmpty()) {
                    Multiaddr address = addresses.get(0);
                    serviceInfo.setAttribute(Protocol.DNSADDR.getType(), address.toString());
                    LogUtils.error(TAG, Protocol.DNSADDR.getType() + "=" + address);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
            serviceInfo.setServiceName(ownServiceName);
            serviceInfo.setServiceType(DOCS.SERVICE);
            serviceInfo.setPort(server.getPort());

            nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD,
                    RegistrationService.getInstance());


            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> docs.initPinsPage(InitApplication.getTitle(getApplicationContext())));
        } catch (Throwable throwable) {
            events.fatal(getString(R.string.fatal_error,
                    throwable.getClass().getSimpleName(),
                    "" + throwable.getMessage()));
            unRegisterService();
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unRegisterService();
    }

    private void unRegisterService() {
        try {
            if (nsdManager != null) {
                nsdManager.unregisterService(RegistrationService.getInstance());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
