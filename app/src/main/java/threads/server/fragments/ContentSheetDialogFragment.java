package threads.server.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Objects;

import threads.lite.cid.IPV;
import threads.lite.core.Server;
import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.services.MimeTypeService;

public class ContentSheetDialogFragment extends BottomSheetDialogFragment {

    public static final String TAG = ContentSheetDialogFragment.class.getSimpleName();


    public static ContentSheetDialogFragment newInstance(@NonNull Uri uri) {
        Bundle bundle = new Bundle();
        bundle.putString(DOCS.URL, uri.toString());
        ContentSheetDialogFragment fragment = new ContentSheetDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        dialog.setContentView(R.layout.content_info);


        Bundle bundle = getArguments();
        Objects.requireNonNull(bundle);
        String title = getString(R.string.information);
        String url = bundle.getString(DOCS.URL, "");

        TextView textViewTitle = dialog.findViewById(R.id.title);
        Objects.requireNonNull(textViewTitle);
        textViewTitle.setText(title);

        ImageView imageView = dialog.findViewById(R.id.uri_qrcode);
        Objects.requireNonNull(imageView);
        TextView page = dialog.findViewById(R.id.page);
        Objects.requireNonNull(page);

        if (url.isEmpty()) {
            page.setVisibility(View.GONE);
        } else {
            page.setText(url);
        }

        TextView important = dialog.findViewById(R.id.important);
        Objects.requireNonNull(important);

        TextView reachability = dialog.findViewById(R.id.reachability);
        Objects.requireNonNull(reachability);

        TextView textImportant = dialog.findViewById(R.id.text_important);
        Objects.requireNonNull(textImportant);


        try {
            DOCS docs = DOCS.getInstance(requireContext());
            Server server = docs.getServer();
            Objects.requireNonNull(server);
            long port = server.getPort();
            Bitmap bitmapRight = MimeTypeService.getPortBitmap(requireContext(), port);

            IPV ipv = docs.ipv();
            Bitmap bitmapLeft = MimeTypeService.getIPvBitmap(requireContext(), ipv);

            Drawable drawableLeft = new BitmapDrawable(getResources(), bitmapLeft);
            Drawable drawableRight = new BitmapDrawable(getResources(), bitmapRight);
            important.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    drawableLeft, null, drawableRight, null);


            DOCS.Reachability ddd = docs.getReachability();
            reachability.setText(getString(R.string.reachability, ddd.name()));


            textImportant.setText(getString(R.string.connection_text, ipv.name()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


        Bitmap bitmap = MimeTypeService.getBitmap(url);
        imageView.setImageBitmap(bitmap);

        return dialog;
    }
}
