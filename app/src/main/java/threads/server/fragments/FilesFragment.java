package threads.server.fragments;


import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;
import androidx.work.WorkManager;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigationrail.NavigationRailView;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.lite.cid.Multiaddr;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;
import threads.server.core.files.FileInfo;
import threads.server.model.LiteViewModel;
import threads.server.services.LiteService;
import threads.server.services.MimeTypeService;
import threads.server.utils.FileItemDetailsLookup;
import threads.server.utils.FilesItemKeyProvider;
import threads.server.utils.FilesViewAdapter;
import threads.server.work.UploadFolderWorker;


public class FilesFragment extends Fragment implements FilesViewAdapter.FilesAdapterListener {

    private static final String TAG = FilesFragment.class.getSimpleName();
    private final ArrayDeque<Long> stack = new ArrayDeque<>();
    private boolean widthMode = false;
    private long lastClickTime = 0;
    private LiteViewModel liteViewModel;
    private final ActivityResultLauncher<Intent> mFolderImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);

                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    int items = mClipData.getItemCount();
                                    if (items > 0) {
                                        for (int i = 0; i < items; i++) {
                                            ClipData.Item item = mClipData.getItemAt(i);
                                            Uri uri = item.getUri();

                                            if (!DOCS.hasReadPermission(requireContext(), uri)) {
                                                EVENTS.getInstance(requireContext()).error(
                                                        getString(R.string.file_has_no_read_permission));
                                                return;
                                            }

                                            if (DOCS.isPartial(requireContext(), uri)) {
                                                EVENTS.getInstance(requireContext()).error(
                                                        getString(R.string.file_not_valid));
                                                return;
                                            }

                                            UploadFolderWorker.load(requireContext(),
                                                    getParentFile(), uri);
                                        }
                                    }
                                } else {
                                    Uri uri = data.getData();
                                    if (uri != null) {
                                        if (!DOCS.hasReadPermission(requireContext(), uri)) {
                                            EVENTS.getInstance(requireContext()).error(
                                                    getString(R.string.file_has_no_read_permission));
                                            return;
                                        }

                                        if (DOCS.isPartial(requireContext(), uri)) {
                                            EVENTS.getInstance(requireContext()).error(
                                                    getString(R.string.file_not_valid));
                                            return;
                                        }

                                        UploadFolderWorker.load(requireContext(),
                                                getParentFile(), uri);
                                    }
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private final ActivityResultLauncher<Intent> mFilesImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);

                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();

                                    LiteService.files(requireContext(), mClipData,
                                            getParentFile());

                                } else if (data.getData() != null) {
                                    Uri uri = data.getData();
                                    Objects.requireNonNull(uri);
                                    if (!DOCS.hasReadPermission(requireContext(), uri)) {
                                        EVENTS.getInstance(requireContext()).error(
                                                getString(R.string.file_has_no_read_permission));
                                        return;
                                    }

                                    if (DOCS.isPartial(requireContext(), uri)) {
                                        EVENTS.getInstance(requireContext()).error(
                                                getString(R.string.file_not_valid));
                                        return;
                                    }

                                    LiteService.file(requireContext(), getParentFile(), uri);
                                }

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private FilesViewAdapter filesViewAdapter;
    private RecyclerView recyclerView;
    private ActionMode actionMode;
    private SelectionTracker<Long> selectionTracker;
    private NavigationRailView navigationRailView;
    private ExtendedFloatingActionButton extendedFloatingActionButton;
    private FloatingActionButton floatingActionButton;


    @Override
    public void onDetach() {
        super.onDetach();
        releaseActionMode();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (selectionTracker != null) {
            selectionTracker.onSaveInstanceState(outState);
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.files_view, container, false);
    }

    private void switchDisplayModes() {
        if (!widthMode) {
            navigationRailView.setVisibility(View.GONE);
        } else {
            navigationRailView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        try {
            evaluateDisplayModes();
            switchDisplayModes();
            showFab(Objects.requireNonNull(liteViewModel.getShowFab().getValue()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void clickFilesAdd() {

        try {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType(MimeTypeService.ALL);
            String[] mimeTypes = {MimeTypeService.ALL};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            mFilesImportForResult.launch(intent);

        } catch (Throwable throwable) {
            EVENTS.getInstance(requireContext()).warning(
                    getString(R.string.no_activity_found_to_handle_uri));
            LogUtils.error(TAG, throwable);
        }
    }

    private void showFab(boolean showFab) {
        if (widthMode) {
            extendedFloatingActionButton.hide();
            floatingActionButton.show();
        } else {
            if (showFab) {
                extendedFloatingActionButton.show();
            } else {
                extendedFloatingActionButton.hide();
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        evaluateDisplayModes();

        extendedFloatingActionButton = view.findViewById(R.id.extended_floating_action_add_files);
        extendedFloatingActionButton.setOnClickListener((v) -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            clickFilesAdd();
        });


        floatingActionButton = view.findViewById(R.id.floating_action_add_files);
        floatingActionButton.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            clickFilesAdd();
        });


        navigationRailView = view.findViewById(R.id.navigation_rail);
        switchDisplayModes();


        navigationRailView.setOnItemSelectedListener(item -> {
            int id = item.getItemId();
            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            if (id == R.id.menu_new_text) {
                try {
                    TextDialogFragment.newInstance(liteViewModel.getParentFileIdxValue()).
                            show(getChildFragmentManager(), TextDialogFragment.TAG);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_import_folder) {
                try {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                    mFolderImportForResult.launch(intent);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_new_folder) {
                try {
                    NewFolderDialogFragment.newInstance(liteViewModel.getParentFileIdxValue()).
                            show(getChildFragmentManager(), NewFolderDialogFragment.TAG);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return true;
            } else if (id == R.id.menu_multiaddrs) {
                try {
                    showMultiaddrs();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
            return false;
        });

        TextInputLayout dropDownDirectoryMenu = view.findViewById(R.id.dropdown_directory_menu);


        liteViewModel = new ViewModelProvider(requireActivity()).get(LiteViewModel.class);

        liteViewModel.getShowFab().observe(getViewLifecycleOwner(), (showFab) -> {
            try {
                if (showFab != null) {
                    showFab(showFab);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        liteViewModel.getParentFileIdx().observe(getViewLifecycleOwner(), (parent) -> {
            if (parent != null) {
                if (parent == 0L) {
                    dropDownDirectoryMenu.setVisibility(View.GONE);
                } else {
                    dropDownDirectoryMenu.setVisibility(View.VISIBLE);
                    fillDropDownDirectory(dropDownDirectoryMenu, parent);
                }
            }
        });


        recyclerView = view.findViewById(R.id.recycler_files);


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setItemAnimator(null);

        filesViewAdapter = new FilesViewAdapter(requireContext(), this);
        recyclerView.setAdapter(filesViewAdapter);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                boolean hasSelection = selectionTracker.hasSelection();
                if (dy > 0) {
                    liteViewModel.setShowFab(false);
                } else if (dy < 0 && !hasSelection) {
                    liteViewModel.setShowFab(true);
                }
            }
        });

        FileItemDetailsLookup fileItemDetailsLookup = new FileItemDetailsLookup(recyclerView);


        selectionTracker = new SelectionTracker.Builder<>(TAG, recyclerView,
                new FilesItemKeyProvider(filesViewAdapter),
                fileItemDetailsLookup,
                StorageStrategy.createLongStorage())
                .build();


        selectionTracker.addObserver(new SelectionTracker.SelectionObserver<>() {
            @Override
            public void onSelectionChanged() {
                if (!selectionTracker.hasSelection()) {
                    if (actionMode != null) {
                        actionMode.finish();
                    }
                } else {
                    if (actionMode == null) {
                        actionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (actionMode != null) {
                    actionMode.setTitle("" + selectionTracker.getSelection().size());
                }
                super.onSelectionChanged();
            }

            @Override
            public void onSelectionRestored() {
                if (!selectionTracker.hasSelection()) {
                    if (actionMode != null) {
                        actionMode.finish();
                    }
                } else {
                    if (actionMode == null) {
                        actionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (actionMode != null) {
                    actionMode.setTitle("" + selectionTracker.getSelection().size());
                }
                super.onSelectionRestored();
            }
        });

        filesViewAdapter.setSelectionTracker(selectionTracker);


        liteViewModel.getLiveDataFiles().observe(getViewLifecycleOwner(), (fileInfos) -> {
            if (fileInfos != null) {
                try {
                    int size = filesViewAdapter.getItemCount();
                    boolean scrollToTop = size < fileInfos.size();

                    filesViewAdapter.updateData(fileInfos);

                    if (scrollToTop) {
                        try {
                            recyclerView.scrollToPosition(0);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        });


        if (savedInstanceState != null) {
            selectionTracker.onRestoreInstanceState(savedInstanceState);
        }

    }

    private void fillDropDownDirectory(TextInputLayout dropDownDirectoryMenu, long directory) {


        try {
            FILES files = FILES.getInstance(requireContext());
            List<FileInfo> ancestors = files.getAncestors(directory);
            int size = ancestors.size();

            String[] names = new String[size + 1];
            long[] indices = new long[size + 1];

            names[0] = InitApplication.getTitle(requireContext());
            indices[0] = 0L;

            for (int i = 0; i < size; i++) {
                FileInfo fileInfo = ancestors.get(i);
                names[i + 1] = fileInfo.getName();
                indices[i + 1] = fileInfo.getIdx();
            }

            MaterialAutoCompleteTextView textView = (MaterialAutoCompleteTextView)
                    dropDownDirectoryMenu.getEditText();
            Objects.requireNonNull(textView);
            textView.setSimpleItems(names);

            textView.setText(names[size], false);

            textView.setOnItemClickListener((parent, view, position, id) -> {
                long idx = indices[position];
                if (idx != directory) {
                    liteViewModel.setParentFileIdx(idx);
                }
                textView.dismissDropDown();
            });
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private void showMultiaddrs() throws Exception {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        builder.setTitle(R.string.multiaddrs);

        String link = "";
        DOCS docs = DOCS.getInstance(requireContext());
        for (Multiaddr multiaddr : docs.dialableAddresses()) {
            link = link.concat("\n").concat(multiaddr.toString()).concat("\n");
        }
        builder.setMessage(link);

        builder.setPositiveButton(getString(android.R.string.ok),
                (dialogInterface, which) -> dialogInterface.cancel());
        String finalLink = link;
        builder.setNeutralButton(getString(android.R.string.copy),
                (dialogInterface, which) -> {
                    LogUtils.error(TAG, finalLink);
                    ClipboardManager clipboardManager = (ClipboardManager)
                            requireContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText(
                            getString(R.string.multiaddrs), finalLink);
                    clipboardManager.setPrimaryClip(clipData);
                });
        builder.show();
    }

    private long[] convert(Selection<Long> entries) {
        int i = 0;

        long[] basic = new long[entries.size()];
        for (Long entry : entries) {
            basic[i] = entry;
            i++;
        }

        return basic;
    }

    private void deleteAction() {

        final EVENTS events = EVENTS.getInstance(requireContext());

        if (!selectionTracker.hasSelection()) {
            events.warning(getString(R.string.no_marked_file_delete));
            return;
        }


        try {
            long[] entries = convert(selectionTracker.getSelection());

            selectionTracker.clearSelection();

            removeFiles(entries);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private void removeFiles(long... indices) {

        FILES files = FILES.getInstance(requireContext());
        EVENTS events = EVENTS.getInstance(requireContext());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            long start = System.currentTimeMillis();

            try {
                files.setDeleting(indices);

                events.delete(Arrays.toString(indices));

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }

        });
    }


    @Override
    public void invokeAction(@NonNull FileInfo fileInfo, @NonNull View view) {

        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();

        try {
            PopupMenu menu = new PopupMenu(requireContext(), view);
            menu.inflate(R.menu.popup_file_menu);
            menu.getMenu().findItem(R.id.popup_share).setVisible(true);
            menu.getMenu().findItem(R.id.popup_delete).setVisible(true);


            menu.setOnMenuItemClickListener((item) -> {

                if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                if (item.getItemId() == R.id.popup_info) {
                    clickFileInfo(fileInfo);
                    return true;
                } else if (item.getItemId() == R.id.popup_delete) {
                    clickFileDelete(fileInfo.getIdx());
                    return true;
                } else if (item.getItemId() == R.id.popup_share) {
                    clickFileShare(fileInfo.getIdx());
                    return true;
                } else {
                    return false;
                }
            });

            menu.show();


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


    }

    private void clickFileShare(long idx) {
        EVENTS events = EVENTS.getInstance(requireContext());
        FILES files = FILES.getInstance(requireContext());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            try {
                FileInfo fileInfo = files.getFileInfo(idx);
                Objects.requireNonNull(fileInfo);

                Uri uri = DOCS.getInstance(requireContext()).getIpnsUri(fileInfo);

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, uri.toString());
                intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
                intent.putExtra(Intent.EXTRA_SUBJECT, fileInfo.getName());
                intent.putExtra(Intent.EXTRA_TITLE, fileInfo.getName());


                Intent chooser = Intent.createChooser(intent, getText(R.string.share));
                chooser.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                chooser.addFlags(Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT);
                startActivity(chooser);


            } catch (Throwable ignore) {
                events.warning(getString(R.string.no_activity_found_to_handle_uri));
            }
        });


    }


    private ActionMode.Callback createActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_files_action_mode, menu);

                liteViewModel.setShowFab(false);

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                int itemId = item.getItemId();
                if (itemId == R.id.action_mode_mark_all) {

                    if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                        return true;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();

                    filesViewAdapter.selectAll();

                    return true;
                } else if (itemId == R.id.action_mode_delete) {

                    if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                        return true;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();

                    deleteAction();

                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

                selectionTracker.clearSelection();

                liteViewModel.setShowFab(true);

                if (actionMode != null) {
                    actionMode = null;
                }
            }
        };

    }

    @Override
    public void onClick(@NonNull FileInfo fileInfo) {

        if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();

        try {
            if (!selectionTracker.hasSelection()) {

                if (actionMode != null) {
                    actionMode.finish();
                    actionMode = null;
                }

                if (fileInfo.isDir()) {
                    stack.push(fileInfo.getParent());
                    liteViewModel.setParentFileIdx(fileInfo.getIdx());
                } else {
                    DOCS docs = DOCS.getInstance(requireContext());

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(docs.getIpnsUri(fileInfo));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT);
                    startActivity(intent);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private void clickFileInfo(@NonNull FileInfo fileInfo) {

        Executors.newSingleThreadExecutor().execute(() -> {
            try {
                DOCS docs = DOCS.getInstance(requireContext());
                Uri uri = docs.getIpnsUri(fileInfo);

                if (!widthMode) {
                    ContentSheetDialogFragment.newInstance(uri)
                            .show(getChildFragmentManager(), ContentSheetDialogFragment.TAG);
                } else {
                    ContentDialogFragment.newInstance(uri)
                            .show(getChildFragmentManager(), ContentDialogFragment.TAG);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


    }

    private void evaluateDisplayModes() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity());

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;
        widthMode = widthDp >= 600;
    }

    @Override
    public void invokePauseAction(@NonNull FileInfo fileInfo) {

        if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();


        UUID uuid = fileInfo.getWorkUUID();
        if (uuid != null) {
            WorkManager.getInstance(requireContext()).cancelWorkById(uuid);
        }

        FILES files = FILES.getInstance(requireContext());
        Executors.newSingleThreadExecutor().submit(() ->
                files.resetLeaching(fileInfo.getIdx()));

    }


    private void clickFileDelete(long idx) {
        removeFiles(idx);
    }

    public void releaseActionMode() {
        try {
            if (isResumed()) {
                if (actionMode != null) {
                    actionMode.finish();
                    actionMode = null;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private long getParentFile() {
        return liteViewModel.getParentFileIdxValue();
    }

    public boolean onBackPressed() {
        if (!stack.isEmpty()) {
            liteViewModel.setParentFileIdx(stack.pop());
            return true;
        }
        return false;
    }
}
