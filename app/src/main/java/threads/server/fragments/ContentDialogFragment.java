package threads.server.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.Objects;

import threads.lite.cid.IPV;
import threads.lite.core.Server;
import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.services.MimeTypeService;

public class ContentDialogFragment extends DialogFragment {

    public static final String TAG = ContentDialogFragment.class.getSimpleName();

    public static ContentDialogFragment newInstance(@NonNull Uri uri) {
        Bundle bundle = new Bundle();
        bundle.putString(DOCS.URL, uri.toString());
        ContentDialogFragment fragment = new ContentDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = LayoutInflater.from(requireActivity());
        View view = inflater.inflate(R.layout.dialog_info, null);


        Bundle bundle = getArguments();
        Objects.requireNonNull(bundle);
        String title = getString(R.string.information);
        String url = bundle.getString(DOCS.URL, "");

        ImageView imageView = view.findViewById(R.id.uri_qrcode);
        Objects.requireNonNull(imageView);
        TextView page = view.findViewById(R.id.page);
        Objects.requireNonNull(page);

        if (url.isEmpty()) {
            page.setVisibility(View.GONE);
        } else {
            page.setText(url);
        }

        TextView important = view.findViewById(R.id.important);
        Objects.requireNonNull(important);

        TextView reachability = view.findViewById(R.id.reachability);
        Objects.requireNonNull(reachability);

        TextView textImportant = view.findViewById(R.id.text_important);
        Objects.requireNonNull(textImportant);


        try {
            DOCS docs = DOCS.getInstance(requireContext());
            Server server = docs.getServer();
            Objects.requireNonNull(server);
            long port = server.getPort();
            Bitmap bitmapRight = MimeTypeService.getPortBitmap(requireContext(), port);

            IPV ipv = docs.ipv();
            Bitmap bitmapLeft = MimeTypeService.getIPvBitmap(requireContext(), ipv);

            Drawable drawableLeft = new BitmapDrawable(getResources(), bitmapLeft);
            Drawable drawableRight = new BitmapDrawable(getResources(), bitmapRight);
            important.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    drawableLeft, null, drawableRight, null);


            DOCS.Reachability ddd = docs.getReachability();
            reachability.setText(getString(R.string.reachability, ddd.name()));


            textImportant.setText(getString(R.string.connection_text, ipv.name()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


        Bitmap bitmap = MimeTypeService.getBitmap(url);
        imageView.setImageBitmap(bitmap);

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
        return builder.setTitle(title).setView(view).create();
    }
}
