package threads.server;


import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.view.MenuItem.SHOW_AS_ACTION_IF_ROOM;
import static android.view.MenuItem.SHOW_AS_ACTION_NEVER;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.search.SearchBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textview.MaterialTextView;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Multiaddr;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;
import threads.server.fragments.ContentDialogFragment;
import threads.server.fragments.ContentSheetDialogFragment;
import threads.server.fragments.FilesFragment;
import threads.server.fragments.NewFolderDialogFragment;
import threads.server.fragments.NewTitleDialogFragment;
import threads.server.fragments.TextDialogFragment;
import threads.server.model.EventViewModel;
import threads.server.model.LiteViewModel;
import threads.server.services.MimeTypeService;
import threads.server.work.ResetWorker;
import threads.server.work.UploadFolderWorker;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private long lastClickTime = 0;
    private LinearLayout mainLayout;
    private LiteViewModel liteViewModel;
    private final ActivityResultLauncher<Intent> mFolderImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    int items = mClipData.getItemCount();
                                    if (items > 0) {
                                        for (int i = 0; i < items; i++) {
                                            ClipData.Item item = mClipData.getItemAt(i);
                                            Uri uri = item.getUri();

                                            if (!DOCS.hasReadPermission(getApplicationContext(), uri)) {
                                                EVENTS.getInstance(getApplicationContext()).error(
                                                        getString(R.string.file_has_no_read_permission));
                                                return;
                                            }

                                            if (DOCS.isPartial(getApplicationContext(), uri)) {
                                                EVENTS.getInstance(getApplicationContext()).error(
                                                        getString(R.string.file_not_valid));
                                                return;
                                            }

                                            UploadFolderWorker.load(getApplicationContext(),
                                                    getParentFile(), uri);
                                        }
                                    }
                                } else {
                                    Uri uri = data.getData();
                                    if (uri != null) {
                                        if (!DOCS.hasReadPermission(getApplicationContext(), uri)) {
                                            EVENTS.getInstance(getApplicationContext()).error(
                                                    getString(R.string.file_has_no_read_permission));
                                            return;
                                        }

                                        if (DOCS.isPartial(getApplicationContext(), uri)) {
                                            EVENTS.getInstance(getApplicationContext()).error(
                                                    getString(R.string.file_not_valid));
                                            return;
                                        }

                                        UploadFolderWorker.load(getApplicationContext(),
                                                getParentFile(), uri);
                                    }
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private FilesFragment filesFragment;

    private boolean widthMode = false;

    private void evaluateDisplayModes() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(this);

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;
        widthMode = widthDp >= 600;
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        try {
            evaluateDisplayModes();
            invalidateOptionsMenu();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        MenuCompat.setGroupDividerEnabled(menu, true);

        menu.findItem(R.id.action_new_folder).setVisible(!widthMode);
        menu.findItem(R.id.action_import_folder).setVisible(!widthMode);
        menu.findItem(R.id.action_import_folder).setVisible(!widthMode);
        menu.findItem(R.id.action_new_text).setVisible(!widthMode);
        menu.findItem(R.id.action_multiaddrs).setVisible(!widthMode);

        if (widthMode) {
            menu.findItem(R.id.action_share).setShowAsAction(SHOW_AS_ACTION_IF_ROOM);
        } else {
            menu.findItem(R.id.action_share).setShowAsAction(SHOW_AS_ACTION_NEVER);
        }


        MenuItem searchMenuItem = menu.findItem(R.id.action_search);

        SearchView mSearchView = (SearchView) searchMenuItem.getActionView();
        mSearchView.setContentDescription(getString(R.string.search_files));
        mSearchView.setQueryHint(getString(R.string.search_files));

        mSearchView.setIconifiedByDefault(true);
        String query = liteViewModel.getQuery().getValue();
        Objects.requireNonNull(query);
        mSearchView.setQuery(query, true);
        mSearchView.setIconified(query.isEmpty());


        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                liteViewModel.setQuery(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                liteViewModel.setQuery(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();


        if (item.getItemId() == android.R.id.home) {

            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                if (liteViewModel.getParentFileIdxValue() != 0L) {
                    liteViewModel.setParentFileIdx(0L);
                } else {
                    NewTitleDialogFragment.newInstance().
                            show(getSupportFragmentManager(), NewTitleDialogFragment.TAG);
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        } else if (id == R.id.action_search) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }
            lastClickTime = SystemClock.elapsedRealtime();


        } else if (item.getItemId() == R.id.action_new_folder) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();


            NewFolderDialogFragment.newInstance(
                            liteViewModel.getParentFileIdxValue()).
                    show(getSupportFragmentManager(), NewFolderDialogFragment.TAG);

        } else if (item.getItemId() == R.id.action_import_folder) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
            mFolderImportForResult.launch(intent);

        } else if (item.getItemId() == R.id.action_new_text) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            TextDialogFragment.newInstance(liteViewModel.getParentFileIdxValue()).
                    show(getSupportFragmentManager(), TextDialogFragment.TAG);


        } else if (item.getItemId() == R.id.action_multiaddrs) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();
            try {
                showMultiaddrs();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        } else if (item.getItemId() == R.id.action_share) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            try {
                DOCS docs = DOCS.getInstance(getApplicationContext());
                String link = docs.getHomePageUri().toString();

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_link));
                intent.putExtra(Intent.EXTRA_TEXT, link);
                intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
                intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


                Intent chooser = Intent.createChooser(intent, getText(R.string.share));
                chooser.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                chooser.addFlags(Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT);
                startActivity(chooser);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        } else if (item.getItemId() == R.id.action_documentation) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://gitlab.com/remmer.wilts/ipfs-lite"));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT);
                startActivity(intent);

            } catch (Throwable e) {
                EVENTS.getInstance(getApplicationContext()).warning(
                        getString(R.string.no_activity_found_to_handle_uri));
            }

        } else if (item.getItemId() == R.id.action_reset) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();
            MaterialAlertDialogBuilder alertDialog = new MaterialAlertDialogBuilder(MainActivity.this);
            alertDialog.setTitle(getString(R.string.warning));
            alertDialog.setMessage(getString(R.string.reset_application_data));
            alertDialog.setPositiveButton(getString(android.R.string.ok),
                    (dialogInterface, which) -> {
                        ResetWorker.reset(getApplicationContext());
                        liteViewModel.setParentFileIdx(0L);
                        dialogInterface.dismiss();
                    });
            alertDialog.setNeutralButton(getString(android.R.string.cancel),
                    (dialogInterface, which) -> dialogInterface.dismiss());
            alertDialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        boolean result = filesFragment.onBackPressed();
        if (result) {
            return;
        }
        super.onBackPressed();
    }


    private void showMultiaddrs() throws Exception {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(
                MainActivity.this);
        builder.setTitle(R.string.multiaddrs);

        String link = "";
        DOCS docs = DOCS.getInstance(getApplicationContext());
        for (Multiaddr multiaddr : docs.dialableAddresses()) {
            link = link.concat("\n").concat(multiaddr.toString()).concat("\n");
        }
        builder.setMessage(link);

        builder.setPositiveButton(getString(android.R.string.ok),
                (dialogInterface, which) -> dialogInterface.cancel());
        String finalLink = link;
        builder.setNeutralButton(getString(android.R.string.copy),
                (dialogInterface, which) -> {
                    LogUtils.error(TAG, finalLink);
                    ClipboardManager clipboardManager = (ClipboardManager)
                            getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText(
                            getString(R.string.multiaddrs), finalLink);
                    clipboardManager.setPrimaryClip(clipData);
                });
        builder.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        evaluateDisplayModes();

        filesFragment = (FilesFragment) getSupportFragmentManager()
                .findFragmentById(R.id.files_container);

        SearchBar searchBar = findViewById(R.id.search_bar);
        setSupportActionBar(searchBar);
        searchBar.setText(InitApplication.getTitle(getApplicationContext()));
        searchBar.setOnClickListener(v -> {
            try {
                DOCS docs = DOCS.getInstance(getApplicationContext());
                Uri uri = docs.getHomePageUri();
                if (!widthMode) {
                    ContentSheetDialogFragment.newInstance(uri)
                            .show(getSupportFragmentManager(), ContentSheetDialogFragment.TAG);
                } else {
                    ContentDialogFragment.newInstance(uri)
                            .show(getSupportFragmentManager(), ContentDialogFragment.TAG);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        mainLayout = findViewById(R.id.main_layout);

        MaterialTextView offline_mode = findViewById(R.id.offline_mode);

        liteViewModel = new ViewModelProvider(this).get(LiteViewModel.class);


        EventViewModel eventViewModel =
                new ViewModelProvider(this).get(EventViewModel.class);


        eventViewModel.delete().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {

                        String data = content
                                .replace("[", "")
                                .replace("]", "")
                                .trim();

                        String[] parts = data.split(",");

                        long[] idxs = new long[parts.length];
                        for (int i = 0; i < parts.length; i++) {
                            idxs[i] = Long.parseLong(parts[i].trim());
                        }


                        String message;
                        if (idxs.length == 1) {
                            message = getString(R.string.delete_file);
                        } else {
                            message = getString(
                                    R.string.delete_files, "" + idxs.length);
                        }
                        AtomicBoolean deleteThreads = new AtomicBoolean(true);
                        Snackbar snackbar = Snackbar.make(mainLayout, message, Snackbar.LENGTH_LONG);

                        snackbar.setAction(getString(R.string.revert_operation), (view) -> {

                            try {
                                deleteThreads.set(false);
                                FILES files = FILES.getInstance(getApplicationContext());
                                Executors.newSingleThreadExecutor().execute(() ->
                                        files.resetDeleting(idxs));
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            } finally {
                                snackbar.dismiss();
                            }

                        });

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                if (deleteThreads.get()) {
                                    ExecutorService executor = Executors.newSingleThreadExecutor();
                                    executor.execute(() -> {
                                        try {
                                            DOCS.getInstance(getApplicationContext())
                                                    .deleteDocuments(idxs);
                                        } catch (Throwable throwable) {
                                            LogUtils.error(TAG, throwable);
                                        }
                                    });

                                }
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }

                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.error().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mainLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.connections().observe(this, (event) -> {
            try {
                if (event != null) {
                    DOCS docs = DOCS.getInstance(getApplicationContext());
                    int connections = docs.getServer().numServerConnections();
                    LogUtils.error(TAG, "Number of server connections " + connections);
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.reachability().observe(this, (event) -> {
            try {
                if (event != null) {
                    String reachability = event.getContent();
                    String text = "";
                    if (reachability.equals(DOCS.Reachability.LOCAL.name())) {
                        text = getString(R.string.service_local_reachable);
                    } else if (reachability.equals(DOCS.Reachability.GLOBAL.name())) {
                        text = getString(R.string.service_reachable);
                    } else if (reachability.equals(DOCS.Reachability.RELAYS.name())) {
                        text = getString(R.string.service_relays_reachable);
                    }

                    if (!text.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mainLayout, text,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.fatal().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mainLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventViewModel.warning().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mainLayout, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.title().observe(this, event -> {
            try {
                if (event != null) {
                    searchBar.setText(InitApplication.getTitle(getApplicationContext()));
                    eventViewModel.removeEvent(event);

                    // update data
                    DOCS.getInstance(getApplicationContext()).initPinsPage(
                            InitApplication.getTitle(getApplicationContext())
                    );
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
        eventViewModel.online().observe(this, (event) -> {
            try {
                if (event != null) {
                    offline_mode.setVisibility(View.GONE);
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });
        eventViewModel.offline().observe(this, (event) -> {
            try {
                if (event != null) {
                    offline_mode.setVisibility(View.VISIBLE);
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        // show files view as primary fragment
        liteViewModel.setParentFileIdx(0);

    }


    private long getParentFile() {
        return liteViewModel.getParentFileIdxValue();
    }
}