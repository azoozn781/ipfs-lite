package threads.server.services;

import android.content.ClipData;
import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.PrintStream;
import java.util.UUID;
import java.util.concurrent.Executors;

import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;
import threads.server.work.UploadFileWorker;
import threads.server.work.UploadFilesWorker;


public class LiteService {

    private static final String TAG = LiteService.class.getSimpleName();


    public static void file(@NonNull Context context, long parent, @NonNull Uri uri) {

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + uri);


        FILES files = FILES.getInstance(context);
        Executors.newSingleThreadExecutor().submit(() -> {
            try {
                DOCS docs = DOCS.getInstance(context);
                String name = DOCS.getFileName(context, uri);
                String mimeType = DOCS.getMimeType(context, uri);
                long size = DOCS.getFileSize(context, uri);

                long idx = docs.createDocument(parent, mimeType, null,
                        uri, name, size, false, false);

                UUID request = UploadFileWorker.load(context, idx);
                files.setWork(idx, request);

            } catch (Throwable e) {
                EVENTS.getInstance(context).error(
                        context.getString(R.string.file_not_found));
            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }

        });
    }

    public static void files(@NonNull Context context, @NonNull ClipData data, long parent) {

        try {

            int items = data.getItemCount();

            if (items > 0) {

                File file = DOCS.createTempFile(context);

                try (PrintStream out = new PrintStream(file)) {
                    for (int i = 0; i < items; i++) {
                        ClipData.Item item = data.getItemAt(i);
                        Uri uri = item.getUri();
                        out.println(uri.toString());
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                UploadFilesWorker.load(context, parent, file);
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

}
