package threads.server.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.provider.DocumentsContract;
import android.util.TypedValue;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.util.Optional;

import threads.lite.cid.IPV;
import threads.server.LogUtils;
import threads.server.R;
import threads.server.utils.TextDrawable;

public class MimeTypeService {

    public static final String PDF_MIME_TYPE = "application/pdf";
    public static final String OCTET_MIME_TYPE = "application/octet-stream";
    public static final String PLAIN_MIME_TYPE = "text/plain";
    public static final String TORRENT_MIME_TYPE = "application/x-bittorrent";
    public static final String URL_MIME_TYPE = "text/uri-list";
    public static final String DIR_MIME_TYPE = DocumentsContract.Document.MIME_TYPE_DIR;
    public static final String AUDIO = "audio";
    public static final String ALL = "*/*";
    public static final String TEXT = "text";
    public static final String VIDEO = "video";
    public static final String IMAGE = "image";
    public static final String APPLICATION = "application";
    private static final int FONT_SIZE = 25;
    private static final String TAG = MimeTypeService.class.getSimpleName();
    private static final int QR_CODE_SIZE = 250;
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    public static int getMediaResource(@NonNull String mimeType) {

        if (!mimeType.isEmpty()) {
            if (mimeType.equals(URL_MIME_TYPE)) {
                return R.drawable.bookmark_outline;
            }
            if (mimeType.equals(TORRENT_MIME_TYPE)) {
                return R.drawable.arrow_up_down_bold;
            }
            if (mimeType.equals(OCTET_MIME_TYPE)) {
                return R.drawable.file_star;
            }
            if (mimeType.equals(PLAIN_MIME_TYPE)) {
                return R.drawable.file;
            }
            if (mimeType.startsWith(TEXT)) {
                return R.drawable.file;
            }
            if (mimeType.equals(PDF_MIME_TYPE)) {
                return R.drawable.pdf;
            }
            if (mimeType.equals(DocumentsContract.Document.MIME_TYPE_DIR)) {
                return R.drawable.folder;
            }
            if (mimeType.startsWith(VIDEO)) {
                return R.drawable.file_video;
            }
            if (mimeType.startsWith(IMAGE)) {
                return R.drawable.camera;
            }
            if (mimeType.startsWith(AUDIO)) {
                return R.drawable.audio;
            }
            if (mimeType.startsWith(APPLICATION)) {
                return R.drawable.application;
            }

            return R.drawable.settings;
        }

        return R.drawable.help;

    }

    @NonNull
    public static String getMimeType(@NonNull String name) {
        String mimeType = evaluateMimeType(name);
        if (mimeType != null) {
            return mimeType;
        }
        return OCTET_MIME_TYPE;
    }

    public static Optional<String> getExtension(@Nullable String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    @Nullable
    private static String evaluateMimeType(@NonNull String filename) {
        try {
            Optional<String> extension = getExtension(filename);
            if (extension.isPresent()) {
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.get());
                if (mimeType != null) {
                    return mimeType;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }

    public static int getThemeColorPrimary(@NonNull Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    public static int getThemeColorOnPrimary(@NonNull Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorOnPrimary, value, true);
        return value.data;
    }

    @NonNull
    public static Bitmap getPortBitmap(@NonNull Context context, long port) {

        Canvas canvas = new Canvas();

        int color = getThemeColorPrimary(context);
        int textColor = getThemeColorOnPrimary(context);
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig().textColor(textColor).bold().fontSize(FONT_SIZE).endConfig()
                .buildRoundRect(context.getString(R.string.port,
                        String.valueOf(port)), color, 8);
        Bitmap bitmap = Bitmap.createBitmap(FONT_SIZE * 6, FONT_SIZE * 2,
                Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, FONT_SIZE * 6, FONT_SIZE * 2);

        drawable.draw(canvas);
        return bitmap;
    }

    @NonNull
    public static Bitmap getIPvBitmap(@NonNull Context context, @NonNull IPV ipv) {

        Canvas canvas = new Canvas();
        int color = getThemeColorPrimary(context);
        int textColor = getThemeColorOnPrimary(context);
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig().textColor(textColor).bold().fontSize(FONT_SIZE).endConfig()
                .buildRoundRect(ipv.name(), color, 8);
        Bitmap bitmap = Bitmap.createBitmap(FONT_SIZE * 6, FONT_SIZE * 2,
                Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, FONT_SIZE * 6, FONT_SIZE * 2);

        drawable.draw(canvas);
        return bitmap;
    }

    public static Bitmap getBitmap(@NonNull String content) {
        try {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE,
                    QR_CODE_SIZE, QR_CODE_SIZE);
            return createBitmap(bitMatrix);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public static Bitmap createBitmap(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = matrix.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

}
