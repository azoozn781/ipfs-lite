package threads.server.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.List;
import java.util.Objects;

import threads.server.core.files.FILES;
import threads.server.core.files.FileInfo;
import threads.server.core.files.FileInfoDatabase;

public class LiteViewModel extends AndroidViewModel {


    @NonNull
    private final MutableLiveData<Boolean> showFab = new MutableLiveData<>(true);

    @NonNull
    private final MutableLiveData<Long> parentFile = new MutableLiveData<>(0L);
    @NonNull
    private final MutableLiveData<String> query = new MutableLiveData<>("");
    @NonNull
    private final MediatorLiveData<Void> liveDataMerger = new MediatorLiveData<>();

    private final FileInfoDatabase fileInfoDatabase;

    public LiteViewModel(@NonNull Application application) {
        super(application);

        fileInfoDatabase = FILES.getInstance(
                application.getApplicationContext()).getFileInfoDatabase();

        liveDataMerger.addSource(parentFile, value -> liveDataMerger.setValue(null));
        liveDataMerger.addSource(query, value -> liveDataMerger.setValue(null));

    }

    @NonNull
    public MutableLiveData<String> getQuery() {
        return query;
    }

    public void setQuery(@NonNull String query) {
        getQuery().postValue(query);
    }

    @NonNull
    private String getQueryValue() {
        MutableLiveData<String> queryData = getQuery();
        String query = queryData.getValue();
        String searchQuery = "";
        if (query != null) {
            searchQuery = query.trim();
        }
        if (!searchQuery.startsWith("%")) {
            searchQuery = "%" + searchQuery;
        }
        if (!searchQuery.endsWith("%")) {
            searchQuery = searchQuery + "%";
        }
        return searchQuery;
    }


    @NonNull
    public MutableLiveData<Long> getParentFileIdx() {
        return parentFile;
    }

    public void setParentFileIdx(long idx) {
        getParentFileIdx().postValue(idx);
    }

    @NonNull
    public LiveData<List<FileInfo>> getLiveDataFiles() {
        return Transformations.switchMap(
                liveDataMerger, input -> fileInfoDatabase.fileInfoDao().getLiveDataFiles(
                        getParentFileIdxValue(), getQueryValue()));
    }

    public long getParentFileIdxValue() {
        Long value = getParentFileIdx().getValue();
        return Objects.requireNonNull(value); // this should never be null
    }


    @NonNull
    public MutableLiveData<Boolean> getShowFab() {
        return showFab;
    }

    public void setShowFab(boolean show) {
        getShowFab().postValue(show);
    }
}