package threads.server.core.files;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@androidx.room.Database(entities = {FileInfo.class}, version = 1, exportSchema = false)
@TypeConverters({FileInfo.class})
public abstract class FileInfoDatabase extends RoomDatabase {

    public abstract FileInfoDao fileInfoDao();

}
